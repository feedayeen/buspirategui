'''
Created on Mar 31, 2015

@author: feedayeen
'''
from PyQt4 import QtGui
from PyQt4 import QtCore
from src import Pinout
from src import BusPirate
from random import random
import sys

class Analog(QtGui.QWidget):
    def __init__(self, pirate, buttons):
        super(Analog,self).__init__()
        self.pirate = pirate
        layout = QtGui.QHBoxLayout()
        
        pwmControl = QtGui.QVBoxLayout()
        pwmControl.addWidget(QtGui.QLabel('PWM Control'))
        activatePWM = QtGui.QPushButton('Activate PWM')
        activatePWM.clicked.connect(self.pwmMode)
        activatePWM.setCheckable(True)
        pwmControl.addWidget(activatePWM)
        self.duty = QtGui.QLabel('0 %')
        pwmControl.addWidget(self.duty)
        slider = QtGui.QSlider()
        slider.valueChanged.connect(self.pwmDuty)
        pwmControl.addWidget(slider)
        
        servocontrol = QtGui.QVBoxLayout()
        servocontrol.addWidget(QtGui.QLabel('Servo Control'))
        activateServo = QtGui.QPushButton('Activate Servo')
        activateServo.clicked.connect(self.servoMode)
        activateServo.setCheckable(True)
        pwmControl.addWidget(activateServo)
        self.degree = QtGui.QLabel('90 degrees')
        servocontrol.addWidget(self.degree)
        
        dial = QtGui.QDial()
        dial.setNotchesVisible(True)
        dial.setMinimum(0)
        dial.setMaximum(180)
        dial.setValue(90)
        dial.valueChanged.connect(self.servoDegree)
        servocontrol.addWidget(dial)
        
        layout.addLayout(pwmControl)
        layout.addLayout(servocontrol)
        
        buttons.addButton(activatePWM)
        buttons.addButton(activateServo)
        pinout = Pinout.Pinout()
        pinout.setMaximumWidth(190)
        layout.addWidget(pinout)
        self.setLayout(layout)
    
    def pwmMode(self):
        self.pirate.mode = "PWM"
    
    def pwmDuty(self,val):
        self.duty.setText(str(val) + ' %')
    
    def servoMode(self):
        self.pirate.mode = "Servo"
        
    def servoDegree(self,val):
        self.degree.setText(str(val) + ' degrees')
    
    def reset(self):
        print('reset')
