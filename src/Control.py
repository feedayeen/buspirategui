'''
Created on Mar 31, 2015

@author: feedayeen
'''

from src import *

class Servo:
    def __init__(self):
        self.deg = 90
    
    def changeMode(self,deg):
        self.commands('m','2','S')
        self.updateDeg(deg)
        
    def updateDeg(self,deg):
        deg = clamp(0,deg,180)
        self.deg = deg
        self.commands(str(deg))