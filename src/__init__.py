import serial
from src import BusPirate
from src import GUI


ser = serial.Serial(port=4,
                    baudrate=115200,
                    parity=serial.PARITY_NONE,
                    stopbits=serial.STOPBITS_ONE,
                    bytesize=serial.EIGHTBITS,
                    timeout=1)
print(ser.portstr)
if ser.isOpen():
    ser.close()
ser.open()
bus = BusPirate.BusPirate(ser)
gui = GUI.GUI(bus)
while 1:
    line = input()
    if line == 'exit':
        ser.close()
        exit()
    if line == 'reset':
        bus.reset()
    else:
        print(bus.command(line))
        


def clamp(low,x,high):
    return min(high,max(x,low))