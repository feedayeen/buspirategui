#This section highlights the current I/O configuration for the BusPirate
#A figure will display the connector and indicate which pins are active
#The proper configuration of the I/O lines for communication protocals
# http://dangerousprototypes.com/docs/images/1/1b/Bp-pin-cable-color.png
from PyQt4 import QtGui
from PyQt4 import QtCore
from PyQt4.Qt import *

class Pinout(QtGui.QWidget):
    def __init__(self):
        super(Pinout,self).__init__()
        self.colors = (QtCore.Qt.black,QtCore.Qt.white,
                       QtCore.Qt.gray,QtCore.Qt.darkMagenta,
                       QtCore.Qt.blue,QtCore.Qt.green,
                       QtCore.Qt.yellow,QtGui.QColor(0xFFA500),
                       QtCore.Qt.red,QtGui.QColor(0x7F5217))
        self.default = ('GND','3.3V',
                      '5.0V','ADC',
                      'VPU','AUX',
                      'CLK','MOSI',
                      'CS','MISO')
        self.pins = self.default;
    
    def paintEvent(self, event):
        paint = QtGui.QPainter()
        paint.begin(self)
        paint.fillRect(paint.window(), QtCore.Qt.red)
        paint.fillRect(QRectF(60,30,70,160), QtCore.Qt.darkGray)
        paint.setPen(QtGui.QPen(QtCore.Qt.black, 6, QtCore.Qt.SolidLine))
        paint.drawPolyline(QPointF(130,90),QPointF(130,30),
                           QPointF(60,30),QPointF(60,190),
                           QPointF(130,190),QPointF(130,130))
        font = QtGui.QFont('Arial',12)
        font.setBold(True)
        paint.setFont(font)
        paint.setPen(QtGui.QPen(QtCore.Qt.black, 2, QtCore.Qt.SolidLine))
        for i in range(0,10):
            if i % 2 == 0:
                paint.fillRect(QRectF(70,40+i*15,20,20), self.colors[i])
                paint.drawRect(QRectF(70,40+i*15,20,20))
                paint.drawText(QPointF(10,55+i*15), self.pins[i])
            else:
                paint.fillRect(QRectF(100,25+i*15,20,20), self.colors[i])
                paint.drawRect(QRectF(100,25+i*15,20,20))
                paint.drawText(QPointF(140,40+i*15), self.pins[i])
    
    