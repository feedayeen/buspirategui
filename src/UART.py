'''
Created on Mar 31, 2015

@author: feedayeen
'''

class UART:
    def __init__(self):
        self.baud = (300,1200,2400,4800,9600,19200,38400,57600,115200)
        self.parity = ('8, NONE','8, EVEN','8, ODD','9, NONE')
        self.stop = (1,2)
        self.polarity = ('Idle 1','Idle 0')
        self.output = ('Open drain (H=Hi-Z, L=GND)','Normal (H=3.3V, L=GND)')
        self.settings = (1,1,1,1,1)
    
    def render(self):
        1+1