from PyQt4 import QtCore
from PyQt4 import QtGui
from src import Pinout
from src import Analog
from PyQt4.QtCore import pyqtSlot,SIGNAL,SLOT
from src import BusPirate
import sys

class GUI:
    def __init__(self, pirate):
        app     = QtGui.QApplication(sys.argv)
        tabs    = QtGui.QTabWidget()
        tabs.resize(800, 400)
        tabs.move(300, 300)
        
        buttons = QtGui.QButtonGroup()  #I want the radio-button effect to occur application wide
        buttons.setExclusive(True)
        
        pushButton = QtGui.QPushButton("PushButton")
        pushButton.setCheckable(True)
        buttons.addButton(pushButton)
        
        tab2    = QtGui.QWidget()
        tab3    = QtGui.QWidget()
        
        self.analog = Analog.Analog(pirate,buttons)
        
        vBoxlayout    = QtGui.QVBoxLayout()
        vBoxlayout.addWidget(pushButton)
        tab2.setLayout(vBoxlayout)
        
        layout = QtGui.QVBoxLayout()
        combo = QtGui.QComboBox()
        combo.addItem('bob')
        combo.addItem('sam')
        combo.activated.connect(self.combo)
        layout.addWidget(combo)
        tab3.setLayout(layout)
        
        tabs.addTab(self.analog,"Analog")
        tabs.addTab(tab2,"Tab 2")
        tabs.addTab(tab3,"Tab 3")
        
        tabs.setWindowTitle('BusPirate GUI')
        tabs.connect(tabs, SIGNAL('currentChanged(int)'), self.newTab)
        tabs.show()
        sys.exit(app.exec_())
       
    
    def newTab(self,index):
        self.analog.reset()
        print(index)
    
    def combo(self, text):
        print(text)
        
    
'''
1 = HiZ
2 = 1WIRE
3 = UART -     baud, data/parity, stop, receive, output
        Opens a terminal allowing I/O
        Converter allowing dec,bin,hex conversion
4 = I2C -     speed
        Read/Write registers
        Converter allowing dec,bin,hex conversion
5 = SPI -     speed, clock, edge, sample, cs, output
6 = 2WIRE -    speed,output
7 = 3WIRE -    speed, CS, output
8 = LCD - don't use
9 = DIO
    ADC
        sample, scope
    Frequency
        sample, continues
    PWM
        %dial
    Servo
        angle dial
    states
'''