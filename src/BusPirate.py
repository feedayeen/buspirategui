#This will handle all communications with the BusPirate after COM port connection
#Initialization will request the current version information
#Functions will set the mode and perform all I/O operations
import serial
import time
from distutils.cmd import Command


class BusPirate:
    def __init__(self, ser):
        self.com = ser
        self.mode = "NA"
    
    def reset(self):
        i = 1
        line = self.command('#')
        while i < 10 and 'Firmware' not in line:
            line = self.command('#')
            i = i + 1
        return 'Firmware' in line
    
    def commands(self, *lines):
        self.reset()
        for line in lines:
            print(line)
            self.command(line)
    
    def command(self, line):
        line = line + '\r'
        self.com.write(line.encode())
        buff = ''
        stop = time.time() + 1     #Wait a maximum of 1000ms for data to come in
        while time.time() < stop:
            if self.com.inWaiting() > 0:
                buff += self.com.read(self.com.inWaiting()).decode()
                stop = time.time() + .1    #If data comes in, wait 100ms for additional data
        return buff
    